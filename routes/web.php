<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PlanetController;
use App\Http\Controllers\CharacterController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CharacterController::class, 'index']);
Auth::routes();

Route::get('/fetch-all-data', [HomeController::class, 'fetchAllData'])->name('FetchAllData');

Route::middleware(['auth'])->group(function () {

    Route::prefix('people')->group(function () {
        Route::get('/', [CharacterController::class, 'list'])->name('Characters');
        Route::get('/list', [CharacterController::class, 'CharactersList'])->name('CharactersList');
        Route::get('/{character}',  [CharacterController::class, 'edit'])->name('EditCharacter');
        Route::put('/{character}',  [CharacterController::class, 'update'])->name('UpdateCharacter');
        Route::delete('/{character}',  [CharacterController::class, 'destroy'])->name('DeleteCharacter');
    });
});
Route::prefix('planets')->group(function () {
    Route::get('/', [PlanetController::class, 'index'])->name('Planets');
});
Route::prefix('films')->group(function () {
    Route::get('/', [FilmController::class, 'index'])->name('Films');
    Route::get('/list', [FilmController::class, 'FilmsList'])->name('FilmsList');
});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');