@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 card">
                <div class="card-body">
                    @include('includes.flash_message')
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                 {{ $error }} <br>
                            @endforeach
                        </div>
                    @endif
      
                <table id="planetsTable" class="table table-bordered table-striped table-responsive-md">
                    <thead class="thead-dark">
                        <tr>
                            <th>Name</th>
                            <th>Population</th>
                        
                        </tr>
                    </thead>
                        <tbody>
                            @foreach($planets as $planet)
                                 <tr>
                                    <td>{{$planet->name}}</td>
                                    <td>{{$planet->population}}</td>
                             
                                </tr>
                            @endforeach 
                        </tbody>
                </table>
                </div> 
            </div>
        </div>
    </div>
@endsection