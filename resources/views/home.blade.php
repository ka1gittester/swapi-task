@extends('layouts.app')

@section('content')
@include('includes.flash_message')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-3 col-md-6">
            <div class="small-box bg-info py-4">
                <div class="inner">
                <p class="text-white h3">Fetch Data</p>
                </div>
                <div class="icon">
                <i class="fas fa-tasks"></i>                        
                </div>
                <a href="{{route('FetchAllData')}}" class="small-box-footer py-3">
                Fetch <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="small-box bg-secondary py-4">
                <div class="inner">
                <p class="text-white h3">Characters</p>
                </div>
                <div class="icon">
                <i class="fas fa-tasks"></i>                        
                </div>
                <a href="{{route('Characters')}}" class="small-box-footer py-3">
                Access <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="small-box bg-success py-4">
                <div class="inner">
                <p class="text-white h3">Planets</p>
                </div>
                <div class="icon">
                <i class="fas fa-tasks"></i>                        
                </div>
                <a href="{{route('Planets')}}" class="small-box-footer py-3">
                Access <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="small-box bg-primary py-4">
                <div class="inner">
                <p class="text-white h3">Films</p>
                </div>
                <div class="icon">
                <i class="fas fa-tasks"></i>                        
                </div>
                <a href="{{route('Films')}}" class="small-box-footer py-3">
                Access <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
