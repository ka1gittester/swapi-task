@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 card">
                <div class="card-body">
                    @include('includes.flash_message')
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                 {{ $error }} <br>
                            @endforeach
                        </div>
                    @endif
      
                <table id="charactersTable" class="table table-bordered table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th>Name</th>
                            <th>Height</th>
                            <th>Mass</th>
                            <th>Hair Color</th>
                            <th>Skin Color</th>
                            <th>Eye Color</th>
                            <th>Birth Year</th>
                            <th>Gender</th>
                            <th>Homeworld</th>
                            <th></th>       
                        </tr>
                    </thead>
                     
                </table>
                </div> 
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $("#charactersTable").DataTable({
                serverSide: true,
                ajax: "{{ route('CharactersList') }}",
                responsive: true,
                language: {
                        paginate: {
                            next: "<i class='fas fa-angle-double-right'></i>",
                            previous: "<i class='fas fa-angle-double-left'></i>"
                        }
                    },
                columns: [
                    { name: 'name' },
                    { name: 'height' },
                    { name: 'mass' },
                    { name: 'hair_color' },
                    { name: 'skin_color' },
                    { name: 'eye_color' },
                    { name: 'birth_year' },
                    { name: 'gender' },
                    { name: 'planet_id' },
                    { name: 'action', orderable: false, searchable:false}
                ],
            });
        });
    </script>
@endsection