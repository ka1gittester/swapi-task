@extends('layouts.app')
@section('content')
<div class="row mb-3">
    <div class="col-lg-12">
        <div class="text-center">
            <h2>Swapi Characters</h2>
            <hr>
        </div>
    </div>
</div>
<div class="container-fluid mb-5 justify-content-center">
    <div class="row">
        @foreach ($characters as $character)
            <div class="col-lg-4 col-md-6 mb-4 ">
                <div class="card shadow  py-0">
                    <div class="card-body py-2 px-2">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class=" font-weight-bold mb-4 mt-2 text-primary text-center text-uppercase mb-1">
                                    {{ $character->name }} @if(hasInAllTheFilms($character->id)) <i class="fa fa-star text-danger"></i> @endif
                                </div>
                                <div class="h6 mb-0 text-gray-800 text-center">
                                    <i class="fas fa-igloo"></i> {{ $character->homeworld->name ?? "" }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach      
    </div>
    <div class="d-flex justify-content-center">
        {{$characters->links()}}
    </div>
</div>
@endsection