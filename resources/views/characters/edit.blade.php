@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-8 card">
                <div class="card-body">
                    @include('includes.flash_message')
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                 {{ $error }} <br>
                            @endforeach
                        </div>
                    @endif
                    <h2 class="card-title">Editing Character</h2>
                    <form action="{{route('UpdateCharacter', $character->id)}}" method="post">
                        {{ csrf_field() }}
                        @method('PUT')
                        @include ('characters.form', ['formMode' => 'edit'])
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection