<a href="{{route('EditCharacter', $character->id) }}"><button class="btn btn-info btn-sm"><i class="far fa-eye" aria-hidden="true"></i></a>
<form id="{{$character->id}}" method="POST" action="{{ route('DeleteCharacter', $character->id) }}" accept-charset="UTF-8" style="display:inline">
    {{ method_field('DELETE') }}
    {{ csrf_field() }}
    <button type="button" class="btn btn-danger btn-sm" title="Delete Data" onclick="confirmDelete('{{$character->id}}')"><i class="fas fa-trash"></i></button>
</form> 