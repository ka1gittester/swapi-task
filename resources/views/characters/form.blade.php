<div class="form-group">
    <label for="name">Name</label>
    <input type="text" id="name" name="name" class="form-control" placeholder="Enter Name"  value="{{ isset($character->name) ? $character->name : '' }}">
</div>
<div class="form-group">
    <label for="height">Height</label>
    <input type="text" id="height" name="height" class="form-control" placeholder="Enter Height"  value="{{ isset($character->height) ? $character->height :'' }}">
</div>
<div class="form-group">
    <label for="mass">Mass</label>
    <input type="text" id="mass" name="mass" class="form-control" placeholder="Enter Mass"  value="{{ isset($character->mass) ? $character->mass : '' }}">
</div>
<div class="form-group">
    <label for="hair_color">Hair Color</label>
    <input type="text" id="hair_color" name="hair_color" class="form-control" placeholder="Enter Hair Color"  value="{{ isset($character->hair_color) ? $character->hair_color : ''}}">
</div>
<div class="form-group">
    <label for="skin_color">Skin Color</label>
    <input type="text" id="skin_color" name="skin_color" class="form-control" placeholder="Enter skin Color"  value="{{ isset($character->skin_color) ? $character->skin_color : '' }}">
</div>
<div class="form-group">
    <label for="eye_color">Eye Color</label>
    <input type="text" id="eye_color" name="eye_color" class="form-control" placeholder="Enter eye Color"  value="{{ isset($character->eye_color) ? $character->eye_color : ''}}">
</div>
<div class="form-group">
    <label for="birth_year">Birth Year</label>
    <input type="text" id="birth_year" name="birth_year" class="form-control" placeholder="Enter Birth Year"  value="{{ isset($character->birth_year) ? $character->birth_year : ''}}">
</div>
<div class="form-group">
    <label for="gender">Gender</label>
    <input type="text" id="gender" name="gender" class="form-control" placeholder="Enter Gender"  value="{{ isset($character->gender) ? $character->gender : ''}}">
</div>
<div class="form-group">
    <select class="form-control form-control-md" id="planet_id" name="planet_id">
        <option value="">Homeworld</option>
        @foreach($planets as $key => $value)
            <option value="{{$key}}" {{isset($character->planet_id) && $character->planet_id == $key ?'selected' : ''}}>{{$value}} </option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Add Character' }}">
</div>  
