@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 card">
                <div class="card-body">
                    @include('includes.flash_message')
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                 {{ $error }} <br>
                            @endforeach
                        </div>
                    @endif
                <table id="filmsTable" class="table table-bordered table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th>Title</th>
                            <th>Episode</th>
                        </tr>
                    </thead> 
                </table>
                </div> 
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $("#filmsTable").DataTable({
                serverSide: true,
                ajax: "{{ route('FilmsList') }}",
                responsive: true,
                language: {
                        paginate: {
                            next: "<i class='fas fa-angle-double-right'></i>",
                            previous: "<i class='fas fa-angle-double-left'></i>"
                        }
                    },
                columns: [
                    { name: 'title' },
                    { name: 'episode' },
                ],
            });
        });
    </script>
@endsection