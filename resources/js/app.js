require('./bootstrap');
import Swal from 'sweetalert2';

window.confirmDelete = function(formId)
{
    Swal.fire({
        icon: 'warning',
        text: 'Do you want to delete this?',
        showCancelButton: true,
        confirmButtonText: 'Delete',
        confirmButtonColor: '#e3342f',
    }).then((result) => {
        if (result.isConfirmed) {
            document.getElementById(formId).submit();
        }  else {
            swal(
                'Cancelled',
                'Data is safe.  Bye',
                'error'
            )
        }
    });
}

window.confirmHide = function(formId)
{
    Swal.fire({
        icon: 'warning',
        text: 'Do you want to hide this?',
        showCancelButton: true,
        confirmButtonText: 'Hide data',
        confirmButtonColor: '#e3342f',
    }).then((result) => {
        if (result.isConfirmed) {
            document.getElementById(formId).submit();
        }  else {
            swal(
                'Cancelled',
                'Data is safe.  Bye',
                'error'
            )
        }
    });
}