<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Planet extends Model
{
    protected $guarded = [];

    public function characters()
    {
        return $this->hasMany(Character::class, 'planet_id', 'swapi_planet_id');
    }
}