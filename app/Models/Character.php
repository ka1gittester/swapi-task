<?php

namespace App\Models;

use App\Models\Film;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Character extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'swapi_character_id',
        'name',
        'height',
        'mass',
        'hair_color',
        'skin_color',
        'eye_color',
        'birth_year',
        'gender',
        'planet_id',
    ];

    public function homeworld()
    {
        return $this->belongsTo(Planet::class, 'planet_id', 'swapi_planet_id');
    }


    /**
     * The films that belong to the Character
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function films(): BelongsToMany
    {
        return $this->belongsToMany(Film::class, 'character_film', 'character_id', 'film_id', 'swapi_character_id', 'id');
    }

    /**
     * Display the relationship data in custom column(planet_id).
     *
     */
    public static function laratablesPlanetId($character)
    {
        return $character->homeworld->name;
    }

    public static function laratablesCustomAction($character)
    {
        return view('characters.action', compact('character'))->render();
    }
}