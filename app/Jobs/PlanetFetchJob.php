<?php

namespace App\Jobs;

use App\Models\Planet;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Utils\BaseAPIConnection\BaseAPIConnection;

class PlanetFetchJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $id;
    private $baseAPIConnection;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
        $this->baseAPIConnection = app(BaseAPIConnection::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $planet = $this->baseAPIConnection->getSingleData('planets', $this->id);
        if ($planet) {
            // CREATE PLANET
            Planet::firstOrCreate([
                'swapi_planet_id' => $this->id,
                'name' => $planet->name,
                'population' => $planet->population,
            ]);
        } else {
            Log::debug('No People Found');
        }
    }
}