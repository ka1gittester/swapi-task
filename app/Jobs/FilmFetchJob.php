<?php

namespace App\Jobs;

use App\Models\Film;
use Illuminate\Bus\Queueable;
use App\Jobs\CharacterFetchJob;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Utils\BaseAPIConnection\BaseAPIConnection;

class FilmFetchJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $baseAPIConnection;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->baseAPIConnection = app(BaseAPIConnection::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->processFilmData(1);
    }

    public function processFilmData($page = 1)
    {
        $allFilms = $this->baseAPIConnection->getAllData('films', $page);
        if ($allFilms) {
            foreach ($allFilms->results as $key => $film) {
                // Insert into film
                $tmpFilm = Film::firstOrCreate([
                    'title' => $film->title,
                    'episode' => $film->episode_id,
                ]);

                // Insert into character film
                $charArray = [];
                foreach ($film->characters as $key => $character) {
                    $characterId = getIdFromUrl('people', $character);
                    // Insert into character
                    CharacterFetchJob::dispatch($characterId)->delay(now()->addSeconds(10));
                    array_push($charArray, $characterId);
                }

                // Log::debug(var_dump($charArray));
                $tmpFilm->characters()->sync($charArray);
            }
            if (isset($allFilms->next) && $allFilms->next) {
                $nextPage = explode('?page=', $allFilms->next)[1];
                $this->processFilmData($nextPage);
            }
        } else {
            Log::debug('No Films Found');
        }
    }
}