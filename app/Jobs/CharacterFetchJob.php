<?php

namespace App\Jobs;

use App\Models\Character;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Utils\BaseAPIConnection\BaseAPIConnection;

class CharacterFetchJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $id;
    private $baseAPIConnection;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
        $this->baseAPIConnection = app(BaseAPIConnection::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $character = $this->baseAPIConnection->getSingleData('people', $this->id);
        if ($character) {
            try {
                DB::beginTransaction();
                // CREATE PEOPLE
                $ch = Character::firstOrCreate([
                    'swapi_character_id' => $this->id,
                    'name' => $character->name,
                    'height' => $character->height ?? '',
                    'mass' => $character->mass ?? '',
                    'hair_color' => $character->hair_color,
                    'skin_color' => $character->skin_color,
                    'eye_color' => $character->eye_color,
                    'birth_year' => $character->birth_year,
                    'gender' => $character->gender,
                    'planet_id' => getIdFromUrl('planets', $character->homeworld),
                ]);
                // Insert into planet
                PlanetFetchJob::dispatch($ch->planet_id)->delay(now()->addSeconds(10));
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                Log::debug(json_encode($character, JSON_PRETTY_PRINT));
                //throw $th;
            }
        } else {
            Log::debug('No People Found');
        }
    }
}