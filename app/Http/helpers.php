<?php

use App\Models\Film;
use App\Models\Character;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;



if (!function_exists('getIdFromUrl')) {

    function getIdFromUrl($reource, $str)
    {
        return (int) Str::of(explode("/{$reource}/", $str)[1])->replace('/', '')->__toString();
    }
}



if (!function_exists('hasInAllTheFilms')) {
    function hasInAllTheFilms($id)
    {
        $allFilms = Cache::remember('filmArr', 3600, function () {
            return Film::all()->pluck('id')->toArray();
        });
        // $allFilms = Film::all()->pluck('id')->toArray();

        $characterFilms = Character::find($id)->films()->pluck('id')->toArray();

        Log::debug(json_encode($allFilms, JSON_PRETTY_PRINT));
        Log::debug(json_encode($characterFilms, JSON_PRETTY_PRINT));
        return count($allFilms) == count($characterFilms) ? true : false;
    }
}