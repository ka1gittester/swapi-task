<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use App\Models\Character;
use App\Models\Planet;
use App\Http\Requests\CharacterRequest;
use Freshbitsweb\Laratables\Laratables;
use DB, Log;

class CharacterController extends Controller
{

    public function index()
    {
        $characters = Character::paginate(20);
        return view('characters.index', compact('characters'));
    }
  
    public function list()
    {
        return view('characters.list');
    }
    public function CharactersList()
    {
        return Laratables::recordsOf(Character::class);
    }


    public function edit(Character $character)
    {
        $planets = Planet::pluck('name', 'swapi_planet_id');
        return view('characters.edit', [
            'planets' => $planets,
            'character' => $character,
        ]);
    }

    public function update(Character $character, CharacterRequest $request)
    {

        try {
            DB::beginTransaction();
            $requestData = $request->validated();
            $character->update($requestData);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getCode() . ' - ' . $e->getMessage());
        }
        return redirect()->route('Characters')->with('flash_message', 'Character updated!');
    }

    public function destroy($id)
    {
        $characters = Character::find($id);
        $characters->delete();
        return redirect()->back()->with('flash_message', 'Character deleted!');;
    }
}