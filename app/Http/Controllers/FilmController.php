<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use GuzzleHttp\Client;
use Freshbitsweb\Laratables\Laratables;


class FilmController extends Controller
{
    public function index()
    {
        return view('films.index'); 
    }

    public function FilmsList()
    {
        return Laratables::recordsOf(Film::class);
    }

 
}
