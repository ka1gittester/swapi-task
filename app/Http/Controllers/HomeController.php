<?php

namespace App\Http\Controllers;

use App\Jobs\FilmFetchJob;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fetchAllData()
    {
        // trigger film job for fetch all data
        FilmFetchJob::dispatch()->delay(now()->addSeconds(5));

        return redirect()->back()->with(['flash_message' => 'Fetch Data job is triggered and processing in background']);
    }
}