<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CharacterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
    
        return [
            'name' => 'required',
            'height' => 'nullable',
            'mass' => 'nullable',
            'hair_color' => 'required',
            'skin_color' => 'required',
            'eye_color' => 'required',
            'birth_year' => 'required',
            'gender' => 'nullable',
            'planet_id' => 'required'

        ];
    }
  
       /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Input Name',
            'hair_color.required' => 'Input hair color',
            'skin_color.required' => 'Input skin color',
            'eye_color.required' => 'Input eye color',
            'birth_year.required' => 'Input birth year',
            'planet_id.required' => 'Input homeworld',
        ];
    }
}

    
   
  