<?php

namespace App\Utils\BaseAPIConnection;

use GuzzleHttp\Client;
use App\Utils\Traits\ResourceCallingTrait;

class BaseAPIConnection
{
    private $api_url = '';

    public function __construct()
    {
        $this->api_url = config('services.swapi.api_url');
    }

    use ResourceCallingTrait;

    public function getHttpClient()
    {

        return new Client([
            'base_uri' => $this->api_url . '/'
        ]);
    }
}