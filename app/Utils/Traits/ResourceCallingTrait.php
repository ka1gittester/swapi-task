<?php

namespace App\Utils\Traits;

use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\ClientException;

trait ResourceCallingTrait
{
    // API CALL SECTION FOR ALL DATA
    public function getAllData($resource = 'films', $page = 1)
    {
        try {
            $client = $this->getHttpClient();
            $response = $client->request('GET', "{$resource}/?page={$page}");
            return $results = json_decode($response->getBody()->getContents());
        } catch (ClientException $e) {
            Log::debug($resource . "--->" . $e->getResponse()->getBody()->getContents());
            return false;
        } catch (\Throwable $th) {
            Log::debug($resource . "--->" . $th->getMessage());
            return false;
        }
    }

    // API CALL SECTION FOR SINGLE RESOURCE
    public function getSingleData($resource = 'films', $id = 1)
    {
        try {
            $client = $this->getHttpClient();
            $response = $client->request('GET', "{$resource}/{$id}");
            return $results = json_decode($response->getBody()->getContents());
        } catch (ClientException $e) {
            Log::debug($resource . "--->" . $e->getResponse()->getBody()->getContents());
            return false;
        } catch (\Throwable $th) {
            Log::debug($resource . "--->" . $th->getMessage());
            return false;
        }
    }
}