## About TrainEasy Task

TASK
Use this endpoint https://swapi.dev/api/people/ to access and store the data to local database. Display the data in a CRUD and allow the user to overwrite the stored data. 

We are only interested in characters data. As for Planets, Spaceships and Vehicles, please chose one of them to show the name or title, there's no need to show all details of all fields. For characters that appear in every film, please show an extra icon or banner beside the character’s name:

## Install 
- [Composer Update]
- [Copy .env.example .env]
- [php artisan key:generate]
- [php artisan migrate:fresh --seed]
- npm install & npm run dev
- [php artisan serve]

## App Access
- [login] username: admin@gmail.com  | pwd : password
- Fetch Data


- [Deployed to Heroku](http://swapi-te.herokuapp.com/).





